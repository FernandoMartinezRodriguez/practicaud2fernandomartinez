CREATE DATABASE If not exists baseCompras;

USE baseCompras;

CREATE TABLE IF NOT exists discos(
iddiscos int auto_increment primary key,
nombre varchar(50) not null unique, 
cantante varchar(50),
num_canciones varchar(50),
genero varchar(50),
fechasalida date
);

CREATE TABLE IF NOT exists revistas(
idrevistas int auto_increment primary key,
nombre varchar(50) not null unique,
autor varchar(50),
num_paginas varchar(50),
fechapublicacion date
);

CREATE TABLE IF NOT exists compras(
idcompra int auto_increment primary key,
comprador varchar(50) not null unique,
vendedor varchar(50),
idproducto int not null,
precio double not null,
fechacompra date
);

--
alter table compras
add foreign key (iddiscos) references discos(iddiscos),
add foreign key (idrevistas) references revistas(idrevistas);

--


create function existeCompra(f_compra varchar(40))
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idcompra) from compras)) do
	if ((select comprador from compras 
		 where idcompra=(i+1)) like f_compra) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; 
--

create function existeDisco (f_disco varchar(50))
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(iddiscos) from discos)) do
	if ((select nombre from discos 
	     where iddiscos = (i+1)) like f_disco) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; 
--

create function existeRevista (f_revista varchar(50))
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idrevistas) from revistas)) do
	if ((select nombre from revistas
		where idrevistas = (i+1)) like f_revista)
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end; ||
