package com.ferMartinez.practicaUd2;

public enum Genero {

    GENEROROCKADNROLL("Rock"),
    GENEROPOP("Pop"),
    GENEROJAZZ("Jazz"),
    GENEROSOUL("Soul"),
    GENERORAP("Rap"),
    GENEROFUNK("FUNK"),
    GENEROBLUES("Blues"),
    GENEROMETAL("Metal"),
    GENERODISCO("Disco"),
    GENEROTECNO("Techno"),
    GENEROREGGAE("Reggae");


    private String valor;

    Genero(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
