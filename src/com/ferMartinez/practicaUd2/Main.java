package com.ferMartinez.practicaUd2;

import com.ferMartinez.practicaUd2.gui.Controlador;
import com.ferMartinez.practicaUd2.gui.Modelo;
import com.ferMartinez.practicaUd2.gui.Vista;

public class Main {

    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
