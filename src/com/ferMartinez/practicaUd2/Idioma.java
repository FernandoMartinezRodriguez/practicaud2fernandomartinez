package com.ferMartinez.practicaUd2;

public enum Idioma {

    IDIOMAESPAÑOL("Español"),
    IDIOMAINGLES("Inglés"),
    IDIOMAFRANCES("Francés"),
    IDIOMAITALIANO("Italiano"),
    IDIOMAARABE("Árabe"),
    IDIOMARUSO("Ruso"),
    IDIOMAALEMAN("Alemán"),
    IDIOMACHINO("Chino"),
    IDIOMAGRIEGO("Griego"),
    IDIOMAPORTUGUES("Portugués"),
    IDIOMAJAPONES("Japonés");


    private String valor;

    Idioma(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
