package com.ferMartinez.practicaUd2.gui;

import com.ferMartinez.practicaUd2.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarDiscos();
        refrescarRevistas();
        refrescarCompras();
        refrescar = false;
    }

    private void addActionListeners(ActionListener listener) {
        vista.btnModificarRevsita.addActionListener(listener);
        vista.btnModificarDisco.addActionListener(listener);
        vista.btnModificarCompra.addActionListener(listener);
        vista.btnEliminarCompra.addActionListener(listener);
        vista.btnEliminarDisco.addActionListener(listener);
        vista.btnEliminarRevista.addActionListener(listener);
        vista.btnNuevoCompra.addActionListener(listener);
        vista.btnNuevoDisco.addActionListener(listener);
        vista.btnNuevoRevista.addActionListener(listener);
        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidar.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }


    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.table1.getSelectionModel())) {
                int row = vista.table1.getSelectedRow();
                vista.txtNombreDisco.setText(String.valueOf(vista.table1.getValueAt(row, 1)));
                vista.txtCantanteDisco.setText(String.valueOf(vista.table1.getValueAt(row, 2)));
                vista.txtCancionesDisco.setText(String.valueOf(vista.table1.getValueAt(row, 3)));
                vista.comboGenero.setSelectedItem(String.valueOf(vista.table1.getValueAt(row, 4)));
                vista.fechaDisco.setDate((Date.valueOf(String.valueOf(vista.table1.getValueAt(row, 5)))).toLocalDate());
            } else if (e.getSource().equals(vista.table2.getSelectionModel())) {
                int row = vista.table2.getSelectedRow();
                vista.txtNombreRevista.setText(String.valueOf(vista.table2.getValueAt(row, 1)));
                vista.txtAutorRevista.setText(String.valueOf(vista.table2.getValueAt(row, 2)));
                vista.txtPaginasRevista.setText(String.valueOf(vista.table2.getValueAt(row, 3)));
                vista.comboIdioma.setSelectedItem(String.valueOf(vista.table2.getValueAt(row, 4)));
                vista.fechaRevista.setDate((Date.valueOf(String.valueOf(vista.table2.getValueAt(row, 5)))).toLocalDate());
            } else if (e.getSource().equals(vista.table3.getSelectionModel())) {
                int row = vista.table3.getSelectedRow();
                vista.txtComprador.setText(String.valueOf(vista.table3.getValueAt(row, 1)));
                vista.txtVendedorCompra.setText(String.valueOf(vista.table3.getValueAt(row, 2)));
                vista.comboRevista.setSelectedItem(String.valueOf(vista.table3.getValueAt(row, 3)));
                vista.txtPrecioCompra.setText(String.valueOf(vista.table3.getValueAt(row, 4)));
                vista.fechaCompra.setDate((Date.valueOf(String.valueOf(vista.table3.getValueAt(row, 5)))).toLocalDate());
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.table3.getSelectionModel())) {
                    borrarCamposCompras();
                } else if (e.getSource().equals(vista.table1.getSelectionModel())) {
                    borrarCamposDiscos();
                } else if (e.getSource().equals(vista.table2.getSelectionModel())) {
                    borrarCamposRevistas();
                }
            }
        }
    }

    private DefaultTableModel construirTableModelDiscos(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmDiscos.setDataVector(data, columnNames);

        return vista.dtmDiscos;

    }


    private void refrescarDiscos() {
        try {
            vista.table1.setModel(construirTableModelDiscos(modelo.consultarDisco()));
            vista.comboGenero.removeAllItems();
            for(int i = 0; i < vista.dtmDiscos.getRowCount(); i++) {
                vista.comboGenero.addItem(vista.dtmDiscos.getValueAt(i, 0)+" - "+
                        vista.dtmDiscos.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void refrescarRevistas() {
        try {
            vista.table2.setModel(construirTableModeloRevistas(modelo.consultarRevista()));
            vista.comboIdioma.removeAllItems();
            for(int i = 0; i < vista.dtmRevistas.getRowCount(); i++) {
                vista.comboIdioma.addItem(vista.dtmRevistas.getValueAt(i, 0)+" - "+
                        vista.dtmRevistas.getValueAt(i, 2)+", "+vista.dtmRevistas.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloRevistas(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmRevistas.setDataVector(data, columnNames);

        return vista.dtmRevistas;

    }

    private void refrescarCompras() {
        try {
            vista.table3.setModel(construirTableModelCompras(modelo.consultarCompras()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelCompras(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmCompra.setDataVector(data, columnNames);

        return vista.dtmCompra;

    }

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if(String.valueOf(vista.AdminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.AdminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.txtIP.getText(), vista.optionDialog.txtUser.getText(),
                        String.valueOf(vista.optionDialog.txtpassword.getPassword()), String.valueOf(vista.optionDialog.txtAdminpassword.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "anadirMenu": {
                try {
                    if (comprobarCompraVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table3.clearSelection();
                    } else if (modelo.compraNombreYaExiste(vista.txtComprador.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un menú diferente");
                        vista.table1.clearSelection();
                    } else {
                        modelo.insertarCompra(
                                vista.txtComprador.getText(),
                                vista.txtVendedorCompra.getText(),
                                String.valueOf(vista.comboDisco.getSelectedItem()),
                                String.valueOf(vista.comboRevista.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioCompra.getText()),
                                vista.fechaCompra.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table3.clearSelection();
                }
                borrarCamposCompras();
                refrescarCompras();
            }
            break;
            case "modificarMenu": {
                try {
                    if (comprobarCompraVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table3.clearSelection();
                    } else {
                        modelo.modificarCompra(
                                vista.txtComprador.getText(),
                                vista.txtVendedorCompra.getText(),
                                String.valueOf(vista.comboDisco.getSelectedItem()),
                                String.valueOf(vista.comboRevista.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioCompra.getText()),
                                vista.fechaCompra.getDate(),
                                Integer.parseInt((String)vista.table3.getValueAt(vista.table3.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException | SQLException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table1.clearSelection();
                }
                borrarCamposCompras();
                refrescarCompras();
            }
            break;
            case "eliminarMenu":
                modelo.borrarCompra(Integer.parseInt((String)vista.table3.getValueAt(vista.table3.getSelectedRow(), 0)));
                borrarCamposCompras();
                refrescarCompras();
                break;
            case "anadirPrimerPlato": {
                try {
                    if (comprobarDiscoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table1.clearSelection();
                    } else if (modelo.discoNombreYaExiste(vista.txtNombreDisco.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un disco diferente");
                        vista.table1.clearSelection();
                    } else {
                        modelo.insertarDisco(vista.txtNombreDisco.getText(),
                                vista.txtCantanteDisco.getText(),
                                vista.txtCancionesDisco.getText(),
                                (String) vista.comboGenero.getSelectedItem(),vista.fechaDisco.getDate());
                        refrescarDiscos();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table2.clearSelection();
                }
                borrarCamposDiscos();
            }
            break;
            case "modificarPrimerPlato": {
                try {
                    if (comprobarDiscoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table1.clearSelection();
                    } else {
                        modelo.modificarDisco(vista.txtNombreDisco.getText(), vista.txtCantanteDisco.getText(),
                                vista.txtCancionesDisco.getText(),String.valueOf(vista.comboGenero.getSelectedItem()),vista.fechaDisco.getDate(),
                                Integer.parseInt((String)vista.table1.getValueAt(vista.table1.getSelectedRow(), 0)));
                        refrescarDiscos();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table2.clearSelection();
                }
                borrarCamposDiscos();
            }
            break;
            case "eliminarPrimerPlato":
                modelo.borrarDisco(Integer.parseInt((String)vista.table1.getValueAt(vista.table1.getSelectedRow(), 0)));
                borrarCamposDiscos();
                refrescarDiscos();
                break;
            case "anadirSegundoPlato": {
                try {
                    if (comprobarRevistaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table2.clearSelection();
                    } else if (modelo.revistaNombreYaExiste(vista.txtNombreRevista.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una revista diferente.");
                        vista.table2.clearSelection();
                    } else {
                        modelo.insertarRevista(vista.txtNombreRevista.getText(), vista.txtAutorRevista.getText(),
                                vista.txtPaginasRevista.getText(),
                                vista.fechaRevista.getDate());
                        refrescarRevistas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table2.clearSelection();
                }
                borrarCamposRevistas();
            }
            break;
            case "modificarSegundoPlato": {
                try {
                    if (comprobarRevistaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.table2.clearSelection();
                    } else {
                        modelo.modificarRevista(vista.txtNombreRevista.getText(), vista.txtAutorRevista.getText(),
                                vista.txtPaginasRevista.getText(), vista.fechaRevista.getDate(),
                                Integer.parseInt((String)vista.table2.getValueAt(vista.table2.getSelectedRow(), 0)));
                        refrescarRevistas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.table2.clearSelection();
                }
                borrarCamposRevistas();
            }
            break;
            case "eliminarSegundoPlato":
                modelo.borrarRevista(Integer.parseInt((String)vista.table2.getValueAt(vista.table2.getSelectedRow(), 0)));
                borrarCamposRevistas();
                refrescarRevistas();
                break;
        }
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    private void setOptions() {
        vista.optionDialog.txtIP.setText(modelo.getIP());
        vista.optionDialog.txtUser.setText(modelo.getUser());
        vista.optionDialog.txtPassword.setText(modelo.getPassword());
        vista.optionDialog.txtAdmin.setText(modelo.getAdminPassword());
    }

    private void borrarCamposDiscos() {
        vista.comboGenero.setSelectedIndex(-1);
        vista.txtNombreDisco.setText("");
        vista.txtCantanteDisco.setText("");
        vista.txtCancionesDisco.setText("");
        vista.fechaDisco.setText("");
    }

    private void borrarCamposRevistas() {
        vista.txtNombreRevista.setText("");
        vista.txtAutorRevista.setText("");
        vista.txtPaginasRevista.setText("");
        vista.fechaRevista.setText("");
        vista.comboIdioma.setSelectedIndex(-1);
    }

    private void borrarCamposCompras() {
        vista.txtComprador.setText("");
        vista.txtVendedorCompra.setText("");
        vista.txtPrecioCompra.setText("");
        vista.txtPrecioCompra.setText("");
        vista.fechaCompra.setText("");
    }

    private boolean comprobarDiscoVacio() {
        return vista.txtNombreDisco.getText().isEmpty() ||
                vista.txtCantanteDisco.getText().isEmpty() ||
                vista.txtCancionesDisco.getText().isEmpty() ||
                vista.comboGenero.getSelectedIndex() == -1 ||
                vista.fechaDisco.getText().isEmpty();
    }

    private boolean comprobarRevistaVacia() {
        return vista.txtNombreRevista.getText().isEmpty() ||
                vista.txtAutorRevista.getText().isEmpty() ||
                vista.txtPaginasRevista.getText().isEmpty() ||
                vista.comboIdioma.getSelectedIndex() == -1 ||
                vista.fechaRevista.getText().isEmpty();
    }

    private boolean comprobarCompraVacia() {
        return vista.txtComprador.getText().isEmpty() ||
                vista.txtVendedorCompra.getText().isEmpty() ||
                vista.txtPrecioCompra.getText().isEmpty() ||
                vista.comboRevista.getSelectedIndex() == -1 ||
                vista.fechaCompra.getText().isEmpty();
    }

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
