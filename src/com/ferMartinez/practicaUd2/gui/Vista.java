package com.ferMartinez.practicaUd2.gui;

import com.ferMartinez.practicaUd2.Genero;
import com.ferMartinez.practicaUd2.Idioma;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    JTextField txtNombreRevista;
    JTextField txtAutorRevista;
    JTextField txtPaginasRevista;
    JComboBox comboIdioma;
    JTable table2;
    JButton btnEliminarRevista;
    JButton btnNuevoRevista;
    JButton btnModificarRevsita;
    JTextField txtNombreDisco;
    JTextField txtCantanteDisco;
    JComboBox comboGenero;
    JTable table1;
    JButton btnEliminarDisco;
    JButton btnNuevoDisco;
    JButton btnModificarDisco;
    JTextField txtCancionesDisco;
    JTextField txtComprador;
    JComboBox comboRevista;
    JRadioButton btnNoCompra;
    JTextField txtPrecioCompra;
    JRadioButton btnSiCompra;
    JTable table3;
    JButton btnEliminarCompra;
    JButton btnNuevoCompra;
    JButton btnModificarCompra;
    JLabel lblNombreDisco;
    JLabel lblCantanteDisco;
    JLabel lblCancionesDisco;
    JLabel lblGenero;
    JLabel lblFechasalida;
    DatePicker fechaDisco;
    JLabel lblNombreRevista;
    JLabel lblAutorRevista;
    JLabel lblPaginasRevista;
    JLabel lblIdiomaRevista;
    JLabel lblFechaRevista;
    DatePicker fechaRevista;
    JLabel lblComprador;
    JLabel lblVendedorCompra;
    JTextField txtVendedorCompra;
    JLabel lblRevista;
    JLabel lblDescuento;
    JLabel lblPrecio;
    JLabel lblFechaCompra;
    DatePicker fechaCompra;
    JLabel lblDisco;
    JComboBox comboDisco;

    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmDiscos;
    DefaultTableModel dtmRevistas;
    DefaultTableModel dtmCompra;
    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidar;
    JPasswordField AdminPassword;

    private final static String TITULOFRAME = "PracticaUD2";

    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox1();
        setEnumComboBox2();
        setTableModels();
    }

    private void setTableModels() {

        this.dtmDiscos = new DefaultTableModel();
        this.table1.setModel(dtmDiscos);

        this.dtmRevistas = new DefaultTableModel();
        this.table2.setModel(dtmRevistas);

        this.dtmRevistas = new DefaultTableModel();
        this.table3.setModel(dtmRevistas);
    }

    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setEnumComboBox1() {
        for(Genero constant : Genero.values()) { comboGenero.addItem(constant.getValor()); }
        comboGenero.setSelectedIndex(-1);
    }

    private void setEnumComboBox2() {
        for(Idioma constant : Idioma.values()) { comboIdioma.addItem(constant.getValor()); }
        comboIdioma.setSelectedIndex(-1);
    }


    private void setAdminDialog() {
        btnValidar = new JButton("Validar");
        btnValidar.setActionCommand("abrirOpciones");
        AdminPassword = new JPasswordField();
        AdminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {AdminPassword, btnValidar};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}
