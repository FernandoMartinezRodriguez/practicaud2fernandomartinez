package com.ferMartinez.practicaUd2.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    private Connection conexion;

    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/baseCompras",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    void insertarDisco(String nombre, String cantante, String num_canciones, String genero, LocalDate fechasalida) {
        String sentenciaSql = "INSERT INTO discos (nombre, cantante, num_canciones, genero, fechasalida) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, cantante);
            sentencia.setString(3, num_canciones);
            sentencia.setString(4, genero);
            sentencia.setDate(5, Date.valueOf(fechasalida));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarRevista(String nombre, String autor, String num_paginas, LocalDate fechapublicacion) {
        String sentenciaSql = "INSERT INTO segundoplatos (nombre, autor, num_paginas, fechapublicacion) VALUES (?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, autor);
            sentencia.setString(3, num_paginas);
            sentencia.setDate(4, Date.valueOf(fechapublicacion));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void insertarCompra(String comprador, String vendedor, String disco, String revista, float precio, LocalDate fechacompra) {
        String sentenciaSql = "INSERT INTO menus (comprador, vendedor, iddisco, idrevistas, precio, fechacompra) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        int iddisco = Integer.valueOf(disco.split(" ")[0]);
        int idrevistas = Integer.valueOf(revista.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, comprador);
            sentencia.setString(2, vendedor);
            sentencia.setInt(3, iddisco);
            sentencia.setInt(4, idrevistas);
            sentencia.setFloat(5, precio);
            sentencia.setDate(6, Date.valueOf(fechacompra));
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    void modificarRevista(String nombre, String ingrediente, String acompañamiento, LocalDate fechapublicacion, int idsegundo){

        String sentenciaSql = "UPDATE revistas SET nombre = ?, autor = ?, num_paginas = ?, fechapublicacion = ?" +
                "WHERE idrevistas = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, ingrediente);
            sentencia.setString(3, acompañamiento);
            sentencia.setString(4,  String.valueOf(fechapublicacion));
            sentencia.setInt(5, idsegundo);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarDisco(String nombre, String cantante, String num_canciones, String genero, LocalDate fecha,  int iddiscos){

        String sentenciaSql = "UPDATE discos SET nombre = ?, cantante = ?, num_canciones = ?, genero = ?,fechasalida=?" +
                "WHERE iddiscos = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, cantante);
            sentencia.setString(3, num_canciones);
            sentencia.setDate(4, Date.valueOf(fecha));
            sentencia.setInt(5, iddiscos);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarCompra(String comprador, String vendedor, String disco, String revista,
                       float precio, LocalDate fechacompra, int idcompra) throws SQLException {

        String sentenciaSql = "UPDATE compras SET comprador = ?, vendedor = ?, iddiscos = ?, idrevistas = ?, " +
                "precio = ?, fechacompra = ? WHERE idcompra = ?";
        PreparedStatement sentencia = null;

        int iddiscos = Integer.parseInt(disco.split(" ")[0]);
        int idrevistas = Integer.parseInt(revista.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, comprador);
            sentencia.setString(2, vendedor);
            sentencia.setInt(3, iddiscos);
            sentencia.setInt(4, idrevistas);
            sentencia.setFloat(5, precio);
            sentencia.setDate(6, Date.valueOf(fechacompra));
            sentencia.setInt(7, idcompra);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }

    void borrarRevista(int id) {
        String sentenciaSql = "DELETE FROM revistas WHERE idrevistas = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarDisco(int id) {
        String sentenciaSql = "DELETE FROM discos WHERE iddiscos = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void borrarCompra(int id) {
        String sentenciaSql = "DELETE FROM compras WHERE idcompra = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    ResultSet consultarRevista() throws SQLException {
        String sentenciaSql = "SELECT concat(idrevistas) as 'ID', concat(nombre) as 'Nombre', concat(autor) as 'Autor', " +
                "concat(num_paginas) as 'Nº de páginas', concat(fechapublicacion) as 'Fecha de publicación' FROM revistas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    ResultSet consultarDisco() throws SQLException {
        String sentenciaSql = "SELECT concat(iddiscos) as 'ID', concat(nombre) as 'Nombre', concat(cantante) as 'Cantante', " +
                "concat(num_canciones) as 'Nº de canciones  ', concat(fechasalida) as 'Fecha de salida' FROM discos";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    ResultSet consultarCompras() throws SQLException {
        String sentenciaSql = "SELECT concat(b.idcompra) as 'ID', concat(b.comprador) as 'Comprador', concat(b.vendedor) as 'Vendedor', " +
                "concat(e.idrevistas, ' - ', e.nombre) as 'Revista'," +
                "concat(a.iddiscos, ' - ', a.nombre, ', ', a.nombre) as 'Disco', " +
                "concat(b.precio) as 'Precio', concat(b.fechacompra) as 'Fecha de compra'" +
                " FROM compras as b " +
                "inner join revistas as e on e.idrevistas = b.idrevistas inner join " +
                "discos as a on a.iddiscos = b.iddiscos";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    public boolean compraNombreYaExiste(String nombre) {
        String salesConsult = "SELECT existeCompra(?)";
        PreparedStatement function;
        boolean nombreExists = false;
        try {
            function = conexion.prepareStatement(salesConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreExists;
    }

    public boolean revistaNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeRevistas(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    public boolean discoNombreYaExiste(String nombre) {
        String completeName = nombre;
        String authorNameConsult = "SELECT existeDisco(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(authorNameConsult);
            function.setString(1, completeName);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }
}
